%global _empty_manifest_terminate_build 0
Name:		python-XStatic-Angular-Bootstrap
Version:	2.5.0.0
Release:	1
Summary:	Angular-Bootstrap 2.5.0 (XStatic packaging standard)
License:	MIT
URL:		http://angular-ui.github.io/bootstrap/
Source0:	https://files.pythonhosted.org/packages/d5/3e/01858ce3e6988ee2d41f72d7413a83d525dd838803f59e8191e243884403/XStatic-Angular-Bootstrap-2.5.0.0.tar.gz
BuildArch:	noarch


%description
Angular-Bootstrap JavaScript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package -n python3-XStatic-Angular-Bootstrap
Summary:	Angular-Bootstrap 2.5.0 (XStatic packaging standard)
Provides:	python-XStatic-Angular-Bootstrap
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-XStatic-Angular-Bootstrap
Angular-Bootstrap JavaScript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package help
Summary:	Development documents and examples for XStatic-Angular-Bootstrap
Provides:	python3-XStatic-Angular-Bootstrap-doc
%description help
Angular-Bootstrap JavaScript library packaged for setuptools (easy_install) / pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%prep
%autosetup -n XStatic-Angular-Bootstrap-2.5.0.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-XStatic-Angular-Bootstrap -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Jan 29 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
